package Pruebas01;

import java.util.Scanner;

public class trabajo02 {

    public static void main(String[] args) {

        //declaracion de variable
        int numero1;
        int numero2;
        int resultado;

        Scanner sc =new Scanner(System.in);

        //lectura de datos desde el teclado
        System.out.println("\nIngrese el primer numero a multiplicar: ");
        numero1= sc.nextInt();
        System.out.println("Ingrese el segundo numero a multiplicar: ");
        numero2= sc.nextInt();

        //resultado
        resultado = numero1*numero2;
        System.out.println("\nEl resultado de la multiplicacion es: "+resultado);

    }
}
