import java.util.Scanner;

public class ejer11 {
    public static void main(String[] args) {
        String cargo_trabajador,afiliacion;
        double horasExtras, pagoPorHoras;



        Scanner sc = new Scanner(System.in);

        System.out.println("Ingrese el cargo del trabajador: ");
        cargo_trabajador = sc.nextLine();

        System.out.println("Ingrese horas extras del trabajador: ");
        horasExtras = sc.nextDouble();

        System.out.println("Ingrese su tipo de aporte");
        afiliacion= sc.nextLine();

        pagoPorHoras = 50;
        horasExtras = horasExtras * pagoPorHoras;
       calcularSueldoBase(cargo_trabajador);
       CalcularSueldoNeto(horasExtras,afiliacion, cargo_trabajador);

    }

    private static void calcularSueldoBase(String cargoTrabajador) {
        int sueldo_traba=0;
        switch (cargoTrabajador) {
            case "JEFE":
                sueldo_traba = 5000;
                break;
            case "EMPLEADO":
                sueldo_traba = 3500;
                break;
            case "PRACTICANTE":
                sueldo_traba = 1000;
                break;

        }
            System.out.println("El sueldo base del trabajador es: "+sueldo_traba);
    }

    private static void CalcularSueldoNeto(double horasExtras, String afiliacion, String cargoTrabajador) {
       double sueldo_Neto_trabajador,impuesto_renta;
        double sueldoNeto=0;
        switch (cargoTrabajador) {
            case "JEFE":
                sueldoNeto = 5000;
                break;
            case "EMPLEADO":
                sueldoNeto = 3500 + horasExtras;
                break;
            case "PRACTICANTE":
                sueldoNeto = 1000;
                break;
        }

        double descuentoAfiliacion=0;
        switch (afiliacion) {
            case "AFP":
                descuentoAfiliacion = sueldoNeto * 0.12;
                break;
            case "ONP":
                descuentoAfiliacion = sueldoNeto * 0.13;
                break;
        }
        impuesto_renta=sueldoNeto*0.15;
        sueldo_Neto_trabajador=sueldoNeto-descuentoAfiliacion-impuesto_renta;
        System.out.print("El sueldo final es de " + sueldo_Neto_trabajador);

    }
}
