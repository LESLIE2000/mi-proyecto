package clase_repetitiva;

public class contarDigitos_vercuantasVecesSeRepitr {

    public static void main(String[] args) {

        long numero=457765771;
        long numero_repetido=7;

        System.out.println("La cantidad de dígitos es :"+ ContarDigitos(numero));
        System.out.println("La cantidad de veces que se repite el numero " + numero_repetido + " es : "+ContarRepetido(numero,numero_repetido));


    }

    private static int ContarRepetido(long numero, long nro_a_contar) {
        int contador_repetidos = 0;
        long numero_intermedio = numero;
        long digito_actual = 0;

        do {
            digito_actual = numero_intermedio % 10;

            if (nro_a_contar == digito_actual)
                contador_repetidos++;

            numero_intermedio = (numero_intermedio - digito_actual) / 10;

        } while (numero_intermedio > 0);

        return contador_repetidos;

    }


    private static int ContarDigitos(long numero) {
        int contador = 0;
        long numero_intermedio = numero;
        long digito_actual = 0;

        do {
            digito_actual = numero_intermedio % 10;
            numero_intermedio = (numero_intermedio - digito_actual) / 10;
            contador++;
        } while (numero_intermedio > 0);

        return contador;
    }
}
