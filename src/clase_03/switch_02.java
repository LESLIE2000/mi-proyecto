package clase_03;

import java.util.Locale;

public class switch_02 {
    public static void main(String[] args) {

        String dia="MARTES";

        switch (dia.toLowerCase()) {
            case "lunes":
                System.out.println("Primer dia");
                break;

            case "martes":
                System.out.println("Segundo dia");
                break;
            case "jueves":
                System.out.println("Tercer dia");
                break;
            default:
                System.out.println("Es jueves o dia posterior");
                break;

        }
    }
}
