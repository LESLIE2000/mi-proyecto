package clase_03;

public class metodos {
    public static void main(String[] args) {

        int a=5;
        int b=10;
        int z;

        calcularSuma(a,b);
        z=calcularSumaRetorna(a,b);

        System.out.println("\nLa suma que retorna al main es: "+ z);
    }

    private static int calcularSumaRetorna(int parametro_a, int parametro_b) {
        int d;
        d =parametro_a+parametro_b;
        return d;

    }

    private static void calcularSuma(int parametro_1, int parametro_2) {
        int c;
        c=parametro_1+parametro_2;
        System.out.println("\nLa suma en el método con void es: " + c);
    }


}
