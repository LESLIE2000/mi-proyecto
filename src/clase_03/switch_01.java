package clase_03;

import java.util.Scanner;

public class switch_01 {
    public static void main(String[] args) {
        int a;

        Scanner sc = new Scanner(System.in);

        System.out.println("Ingrese un numero para pasar a letras menor o igual a 5");
        a = sc.nextInt();

        switch(a){
            case 1:System.out.println(" a es uno");
            break;
            case 2:System.out.println("a es dos");
            break;
            case 3:System.out.println("a es tres");
            break;
            case 4: case 5: System.out.println("a es 'cuatro' o 'cinco' ");
            break;
            default: System.out.println("a es mayor a 5");
            break;

        }


    }
}
