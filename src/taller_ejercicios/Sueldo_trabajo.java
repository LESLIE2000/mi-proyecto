package taller_ejercicios;

import java.util.Scanner;

public class Sueldo_trabajo {

    public static void main(String[] args) {

        double sueldo,pagoPorHora,nuevoSueldo;
        int horas;

        Scanner entrada = new Scanner(System.in);

        System.out.println("Ingrese pago por hora ");
        pagoPorHora = entrada.nextDouble();

        System.out.println("Ingrese horas trabajadas");
        horas = entrada.nextInt();

        sueldo = pagoPorHora * horas;

        nuevoSueldo = sueldo - 0.1*sueldo;

        System.out.println("Nuevo sueldo es: " + nuevoSueldo);
    }
}
