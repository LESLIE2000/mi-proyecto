package Practica;

import java.util.Scanner;

public class practica1 {
    static Scanner sc = new Scanner(System.in);
    public static double obtenerSueldoBase(String cargo) {
        return switch (cargo.toLowerCase()) {
            case "jefe" -> 5000;
            case "empleado" -> 3500;
            case "practicante" -> 1000;
            default -> 0;
        };
    }

    public static double calcularSueldoNeto(String cargo, int horasExtras, boolean afiliacionAFP) {
        double sueldoNeto = obtenerSueldoBase(cargo);

        if (!cargo.equalsIgnoreCase("jefe") && !cargo.equalsIgnoreCase("practicante")) {
            double valorHorasExtras = 50 * horasExtras;
            sueldoNeto += valorHorasExtras;
        }


        double descuentoAFP = sueldoNeto * 0.12;
        double descuentoONP = afiliacionAFP ? sueldoNeto * 0.13 : 0;
        sueldoNeto -= descuentoAFP + descuentoONP;

        double descuentoRenta = sueldoNeto * 0.15;
        sueldoNeto -= descuentoRenta;

        return sueldoNeto;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("\nIngrese el cargo ('JEFE''EMPLEADO' 'PRACTICANTE': ");
        String cargo = sc.nextLine();

        System.out.print("\nIngrese las horas extras trabajadas: ");
        int horasExtras = sc.nextInt();

        System.out.print("\n¿Está afiliado a AFP? (true/false): ");
        boolean afiliacionAFP = sc.nextBoolean();

        double sueldoNeto = calcularSueldoNeto(cargo, horasExtras, afiliacionAFP);
        System.out.print("El sueldo final es del " + cargo + " es: " + sueldoNeto);
    }
}

