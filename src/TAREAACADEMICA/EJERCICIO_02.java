package TAREAACADEMICA;

import java.util.Scanner;

public class EJERCICIO_02 {

    public static void main(String[] args) {
        double hora_estacionamiento;
        double cobro_por_hora;
        double total_por_hora ;

        Scanner sc = new Scanner(System.in);

        System.out.println("\nIngrese el precio por hora ");
        cobro_por_hora = sc.nextDouble();

        System.out.println("\nIndique las horas de estacionamiento ");
        hora_estacionamiento = sc.nextDouble();

        total_por_hora = Math.ceil(hora_estacionamiento)* cobro_por_hora;

        System.out.println("\nTotal a cobrar S/ " +total_por_hora);

    }
}
