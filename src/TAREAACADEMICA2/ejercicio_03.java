package TAREAACADEMICA2;

public class ejercicio_03 {

        public static void main(String[] args) {
            evaluarNumeros(5, 10, 3);

        }

        public static void evaluarNumeros(int num1, int num2, int num3) {
            int mayor = obtenerMayor(num1, num2, num3);
            int menor = obtenerMenor(num1, num2, num3);
            int medio = obtenerMedio(num1, num2, num3);

            System.out.println("El mayor es: " + mayor);
            System.out.println("El menor es: " + menor);
            System.out.println("El del medio es: " + medio);
        }

        public static int obtenerMayor(int num1, int num2, int num3) {
            return Math.max(Math.max(num1, num2), num3);
        }

        public static int obtenerMenor(int num1, int num2, int num3) {
            return Math.min(Math.min(num1, num2), num3);
        }

        public static int obtenerMedio(int num1, int num2, int num3) {
            int suma = num1 + num2 + num3;
            int mayor = obtenerMayor(num1, num2, num3);
            int menor = obtenerMenor(num1, num2, num3);

            return suma - mayor - menor;
        }
    }





