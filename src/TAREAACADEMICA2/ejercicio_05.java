package TAREAACADEMICA2;

import java.util.Scanner;

public class ejercicio_05 {
    public static void main(String[] args) {
        double tasa_interes, capital;
        int plazo_anual;


        Scanner entrada = new Scanner(System.in);

        System.out.println("\nIngrese monto del préstamo: ");
        capital = entrada.nextDouble();

        System.out.println("\nIngrese tasa de interés: ");
        tasa_interes = entrada.nextDouble();

        System.out.println("\nIngrese plazo en años: ");
        plazo_anual = entrada.nextInt();

        plazo_anual =plazo_anual*12;
        tasa_interes=tasa_interes/1200;

        double primera_formula= tasa_interes*Math.pow(1+tasa_interes,plazo_anual);
        double segunda_formula=Math.pow(1+tasa_interes,plazo_anual)-1;
        double cuota_calculada=capital*(primera_formula/segunda_formula);
        System.out.printf("\nLa cuota calculada es %.2f ",cuota_calculada);

    }

}
